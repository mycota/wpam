package com.example.wpam

import android.text.format.DateUtils
import org.junit.Test

import org.junit.Assert.*
import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun groupBy() {
        var arr = listOf<Measurement>(
            Measurement(
                "123",
                Instant.parse("2021-11-12T18:23:21.345Z"),
                "618d57fe5fc4027d339786db",
                "618d57fe5fc4027d339786de",
                32.0,
                "1"
            ), Measurement(
                "123",
                Instant.parse("2021-11-11T18:23:21.345Z"),
                "618d57fe5fc4027d339786db",
                "618d57fe5fc4027d339786de",
                31.0,
                "1"
            ),
            Measurement(
                "123",
                Instant.parse("2021-11-11T18:23:21.345Z"),
                "618d57fe5fc4027d339786db",
                "618d57fe5fc4027d339786de",
                32.0,
                "1"
            )
        )
        val res = arr.groupBy { measurement: Measurement ->
            measurement.timestamp.truncatedTo(ChronoUnit.DAYS)
        }
        print(res)
        assertEquals(1, 1)
    }
}