package com.example.wpam

import androidx.room.*
import java.sql.Date
import java.time.Instant

enum class EventType {
    IRRIGATION,
    FERTILIZATION
}

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Instant? {
        return value?.let { Instant.ofEpochMilli(it)}
    }

    @TypeConverter
    fun instantToTimestamp(instant: Instant?): Long? {
        return instant?.toEpochMilli()
    }
}

@Entity
data class Event(
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "timestamp") val timestamp: Instant?,
    @ColumnInfo(name = "type") val type: EventType?,
){
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null
}

@Dao
interface EventDao {
    @Query("SELECT * FROM event")
    fun getAll(): List<Event>

    @Query("SELECT * FROM event WHERE type IN (:type)")
    fun getEventOfType(type: EventType): List<Event>

    @Insert
    fun insertAll(vararg events: Event)

    @Delete
    fun delete(event: Event)
}

@Database(entities = [Event::class], version = 1)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun eventDao(): EventDao
}