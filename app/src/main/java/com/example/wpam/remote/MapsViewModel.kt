package com.example.wpam.remote

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.wpam.*
import com.google.android.gms.maps.model.LatLng
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MapsViewModel : ViewModel() {
    val nearbyPlaces: MutableLiveData<MapsGoogleResponse> = MutableLiveData()


    fun getNearbyPlaces(keyword: String, loc: LatLng, type: String) {
        val apiInterface = IGoogleAPIService.create().getNearbyPlaces(keyword, "${loc.latitude},${loc.longitude}", type, "1500", "AIzaSyCPVRfCPpONKRMCmZw_CW9bAFck3Xwm2Uo")
        apiInterface.enqueue(object : Callback<MapsGoogleResponse> {
            override fun onResponse(
                call: Call<MapsGoogleResponse>,
                response: Response<MapsGoogleResponse>
            ) {

                println(call.request())
                nearbyPlaces.postValue(response.body())
                println("Success")
                //println(response.body())
            }
            override fun onFailure(call: Call<MapsGoogleResponse>, t: Throwable) {
                println("Error")
                t.message?.let { Log.e("TAG", it) }
            }
        })
    }
}