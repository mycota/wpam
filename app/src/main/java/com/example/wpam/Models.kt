package com.example.wpam

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.time.Instant
import java.util.*
import java.io.IOException

import com.google.gson.TypeAdapter
import com.google.gson.annotations.JsonAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonWriter


object InstantSerializer : KSerializer<Instant>, TypeAdapter<Instant?>() {
    override val descriptor = PrimitiveSerialDescriptor("Date", PrimitiveKind.STRING)
    override fun serialize(encoder: Encoder, value: Instant) =
        encoder.encodeString(value.toString())

    override fun deserialize(decoder: Decoder): Instant = Instant.parse(decoder.decodeString())

    @Throws(IOException::class)
    override fun write(out: JsonWriter, value: Instant?) {
        if (value == null) {
            out.nullValue()
            return
        }
        out.value(value.toString())
    }

    @Throws(IOException::class)
    override fun read(input: JsonReader?): Instant? {
        return if (input == null) {
            null
        } else Instant.parse(input.nextString())
    }
}


@Serializable
data class Measurement(
    val ID: String,
    @JsonAdapter(InstantSerializer::class)
    @Serializable(InstantSerializer::class)
    val timestamp: Instant,
    val sensor: String,
    val plant: String,
    val value: Double,
    val custom_plant_id: String
)


enum class SensorType(val sensorName: String){
    TEMPERATURE("temperature"),
    HUMIDITY("humidity"),
    INSOLATION("insolation"),
    HUMIDITYOFSOIL("humidityOfSoil"),
}




@Serializable
data class Sensor(
    val id: String,
    val name: String,
    val unit: String
)