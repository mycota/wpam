package com.example.wpam.ui.information

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.lang.Exception

class SQLiteHelper (context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION){
        companion object  {
            private const val DATABASE_VERSION =1
            private const val DATABASE_NAME = "plant1.id"
            private const val TBL_PLANT = "tbl_plant"
            private const val ID = "id"
            private const val NAME = "name"
            private const val TYPE = "type"
            private const val DATE = "date"
            private const val DESCRIPTION = "description"
        }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTblPlant = ("CREATE TABLE " + TBL_PLANT + "(" + ID + " INTEGER PRIMARY KEY, " + NAME + " TEXT, " + TYPE + " TEXT, " + DATE + " TEXT, " + DESCRIPTION + " TEXT" + ")")
        db?.execSQL(createTblPlant)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TBL_PLANT")
        onCreate(db)
    }

    fun insertPlant(std: PlantModel): Long{
        val db = this.writableDatabase

        val contentValues = ContentValues()
        contentValues.put(ID, std.id)
        contentValues.put(NAME, std.name)
        contentValues.put(TYPE, std.type)
        contentValues.put(DATE, std.date)
        contentValues.put(DESCRIPTION, std.description)

        val success = db.insert(TBL_PLANT, null, contentValues)
        db.close()
        return success
}
    @SuppressLint("Range")
    fun getAllPlants(): ArrayList<PlantModel>{
    val stdList: ArrayList<PlantModel> = ArrayList()
    val selectQuery = "SELECT * FROM $TBL_PLANT"
    val db = this.readableDatabase

    val cursor: Cursor?
    try {
        cursor = db.rawQuery(selectQuery, null)
    }catch (e: Exception){
        e.printStackTrace()
        db.execSQL(selectQuery)
        return ArrayList()
    }
        var id: Int
        var name: String
        var type: String
        var date: String
        var description: String

        if(cursor.moveToFirst()){
            do{
                id = cursor.getInt(cursor.getColumnIndex("id"))
                name = cursor.getString(cursor.getColumnIndex("name"))
                type = cursor.getString(cursor.getColumnIndex("type"))
                date = cursor.getString(cursor.getColumnIndex("date"))
                description = cursor.getString(cursor.getColumnIndex("description"))

                val std = PlantModel(id=id,name=name,type=type,date=date, description=description)
                stdList.add(std)
            }while(cursor.moveToNext())
        }
        return stdList
    }

    fun updatePlant(std: PlantModel): Int{
        val db = this.writableDatabase

        val contentValues = ContentValues()
        contentValues.put(ID,std.id)
        contentValues.put(NAME,std.name)
        contentValues.put(TYPE,std.type)
        contentValues.put(DATE,std.date)
        contentValues.put(DESCRIPTION,std.description)

        val success = db.update(TBL_PLANT,contentValues,"id="+std.id,null)
        db.close()
        return success

    }

    fun deletePlantById(id:Int):Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(ID,id)
        val success = db.delete(TBL_PLANT,"id=$id",null)
        db.close()
        return success
    }
}