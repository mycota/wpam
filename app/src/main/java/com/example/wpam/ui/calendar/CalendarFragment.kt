package com.example.wpam.ui.calendar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.applandeo.materialcalendarview.CalendarView
import com.applandeo.materialcalendarview.CalendarView.ONE_DAY_PICKER
import com.applandeo.materialcalendarview.DatePicker
import com.applandeo.materialcalendarview.builders.DatePickerBuilder
import com.applandeo.materialcalendarview.listeners.OnDayClickListener
import com.applandeo.materialcalendarview.listeners.OnSelectDateListener
import com.example.wpam.R
import com.example.wpam.databinding.FragmentCalendarBinding
import com.example.wpam.ui.chart.ChartViewModel
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.format.DateTimeFormatterBuilder
import java.util.*
import java.util.ArrayList


class CalendarFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var calendarViewModel: CalendarViewModel
    private var _binding: FragmentCalendarBinding? = null
    private val binding get() = _binding!!
    private lateinit var currentDate: Instant


    fun getInstantFromDate(year: Int, month: Int, dayOfMonth: Int): Instant {
        var calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        return Instant.ofEpochMilli(calendar.timeInMillis)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCalendarBinding.inflate(inflater, container, false)
        val view = binding.root



       // binding.calendarView.setHighlightedDays(listOf(GregorianCalendar(2022, 0, 21)))

//        val builder = DatePickerBuilder(this, OnSelectDateListener {
//            println(it)
//        }).setPickerType(CalendarView.ONE_DAY_PICKER)
//        val datePicker: DatePicker = builder.build()
//        datePicker.show();


//        currentDate = Instant.ofEpochMilli(binding.calendarView.date)
//        println(currentDate)
        binding.calendarView.setOnDayClickListener {
            println(Instant.ofEpochMilli(it.calendar.timeInMillis))
            val calendars = listOf(it.calendar)
            binding.calendarView.setHighlightedDays(calendars)
            binding.calendarView.selectedDates = calendars
            binding.calendarView.selectedDates
        }

//        setOnDateChangeListener(CalendarView.OnDateChangeListener { _: CalendarView, year: Int, month: Int, dayOfMonth: Int ->
//            currentDate = getInstantFromDate(year, month, dayOfMonth)
//            println(currentDate)
//        })

        binding.fertilizationBtn.setOnClickListener {
            showActionMessage("fertilized")
        }

        binding.irrigationBtn.setOnClickListener {
            showActionMessage("watered")
        }

            return view
        }

    private fun showActionMessage(action: String) {
        val message = binding.calendarView.selectedDates.firstOrNull()?.let{
            "The plant ${binding.noteEditText.text.toString()} was $action on: ${SimpleDateFormat("dd/MM/yyyy").format(it.time)}"
        } ?: "Pick the date"
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).show()
    }
}