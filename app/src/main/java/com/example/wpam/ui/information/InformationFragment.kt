package com.example.wpam.ui.information

import android.database.sqlite.SQLiteQuery
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.wpam.R
import com.example.wpam.databinding.FragmentInformation2Binding


class InformationFragment : Fragment() {
    private var _binding: FragmentInformation2Binding? = null
    private val binding get() = _binding!!

    private lateinit var edName: EditText
    private lateinit var edType: EditText
    private lateinit var btAdd: Button
    private lateinit var btView: Button

    private lateinit var sqLiteHelper: SQLiteHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentInformation2Binding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        btAdd.setOnClickListener{addPlant()}
        btView.setOnClickListener{getPlants()}
        initView()

        val view = binding.root
        return view
    }

    private fun getPlants() {
        val stdList = sqLiteHelper.getAllPlants()
        Log.e("pppp", "${stdList.size}")
    }

    private fun addPlant(){
        val name = edName.text.toString()
        val type = edType.text.toString()

        if(name.isEmpty() || type.isEmpty()){
            Toast.makeText(activity,"Please enter requried field", Toast.LENGTH_SHORT).show()
        } else{
            val std = PlantModel(name=name, type=type)
            val status = sqLiteHelper.insertPlant(std)
            if(status>-1){
                Toast.makeText(activity, "Plant Added", Toast.LENGTH_SHORT).show()
                clearEditText()
            } else{
                Toast.makeText(activity, "Record not saved", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun clearEditText() {
        edName.setText("")
        edType.setText("")
        edName.requestFocus()
    }


    private fun initView(){
        edName = binding.edName
        edType = binding.edType
        btAdd = binding.btnAdd
        btView = binding.btnView
    }

}