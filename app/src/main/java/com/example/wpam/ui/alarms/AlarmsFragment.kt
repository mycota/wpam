package com.example.wpam.ui.alarms

import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.wpam.SensorType
import com.example.wpam.databinding.FragmentAlarmsBinding
import com.example.wpam.ui.chart.ChartViewModel

class AlarmsFragment : Fragment() {

    private lateinit var alarmsViewModel: AlarmsViewModel
    private var _binding: FragmentAlarmsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAlarmsBinding.inflate(inflater, container, false)

        binding.alarmsContainer.visibility = View.INVISIBLE
        binding.progressBar.visibility = View.VISIBLE

        alarmsViewModel =
            ViewModelProvider(this)[AlarmsViewModel::class.java]

        alarmsViewModel.lastTemperature.observe(viewLifecycleOwner, {
            Log.d("lastTemperature", it.value.toString())
            val round = String.format("%.2f", it.value)
            binding.temp.text = round + "°C"

            binding.temperatureProgressBar.apply {
                progress = it.value.toFloat()
                progressMax = 40f
                setProgressWithAnimation(progress, 1000)
                if (progress < 26 && progress > 21) progressBarColor = Color.GREEN
                if (progress > 26 || progress < 21 && progress > 18) progressBarColor = Color.YELLOW
                if (progress < 18) progressBarColor = Color.RED
            }
        })
        alarmsViewModel.lastHumidity.observe(viewLifecycleOwner, {
            Log.d("lastHumidity", it.value.toString())
            val round = String.format("%.2f", it.value)
            binding.humair.text = round + "%"

            binding.humidityProgressBar.apply {
                progress = it.value.toFloat()
                progressMax = 100f
                setProgressWithAnimation(progress, 1000)
                if (progress > 30) progressBarColor = Color.GREEN
                if (progress < 30) progressBarColor = Color.YELLOW
            }
        })

        alarmsViewModel.lastInsolation.observe(viewLifecycleOwner, {
            Log.d("lastInsolation", it.value.toString())
            binding.ins.text = "${it.value} kWh/m²"

            binding.insolationProgressBar.apply {
                progress = it.value.toFloat()
                progressMax = 15f
                setProgressWithAnimation(progress, 1000)
                progressBarColor = Color.BLUE
            }
        })

        alarmsViewModel.lastHumidityOfSoil.observe(viewLifecycleOwner, {
            Log.d("lastHumidityOfSoil", it.value.toString())
            val round = String.format("%.2f", it.value)
            binding.soil.text = round + "%"

            binding.soilProgressBar.apply {
                progress = it.value.toFloat()
                progressMax = 100f
                setProgressWithAnimation(progress, 1000)
                if (progress > 30) progressBarColor = Color.GREEN
                if (progress < 30) progressBarColor = Color.YELLOW
            }
            binding.progressBar.visibility = View.INVISIBLE
            binding.alarmsContainer.visibility = View.VISIBLE
        })

        alarmsViewModel.getLastMeasurement(SensorType.HUMIDITY)
        alarmsViewModel.getLastMeasurement(SensorType.HUMIDITYOFSOIL)
        alarmsViewModel.getLastMeasurement(SensorType.TEMPERATURE)
        alarmsViewModel.getLastMeasurement(SensorType.INSOLATION)

        val view = binding.root
        return view
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
