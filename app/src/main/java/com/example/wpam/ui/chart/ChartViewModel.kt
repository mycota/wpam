package com.example.wpam.ui.chart

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.wpam.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalAdjuster
import java.time.temporal.TemporalAdjusters
import java.time.ZoneId
import java.time.ZoneOffset

import java.time.ZonedDateTime
import java.util.*


enum class ChartType {
    DAILY,
    WEEKLY,
    MONTHLY
}

class ChartViewModel : ViewModel() {
    public var chartType: ChartType = ChartType.DAILY

    val measurementList: MutableLiveData<List<Measurement>> = MutableLiveData()

    val temperatureList: MutableLiveData<List<Measurement>> = MutableLiveData()
    val humidityList: MutableLiveData<List<Measurement>> = MutableLiveData()
    val insolationList: MutableLiveData<List<Measurement>> = MutableLiveData()
    val humidityOfSoilList: MutableLiveData<List<Measurement>> = MutableLiveData()


    val errorMessage = MutableLiveData<String>()
    val sensorList: MutableLiveData<List<Sensor>> = MutableLiveData()

    fun dailyStatistic(sensorType: SensorType, response: Response<List<Measurement>>) {
        when (sensorType) {
            SensorType.HUMIDITY -> humidityList.postValue(
                response.body()?.reversed()
                    ?.takeLast(24)
            )
            SensorType.TEMPERATURE -> temperatureList.postValue(
                response.body()?.reversed()?.takeLast(24)
            )
            SensorType.INSOLATION -> insolationList.postValue(
                response.body()?.reversed()?.takeLast(24)
            )
            SensorType.HUMIDITYOFSOIL -> humidityOfSoilList.postValue(
                response.body()
                    ?.reversed()?.takeLast(24)
            )
        }
    }

    fun weeklyStatistic(sensorType: SensorType, response: Response<List<Measurement>>) {
        val res = response.body()?.reversed()?.groupBy { measurement: Measurement ->
            measurement.timestamp.truncatedTo(ChronoUnit.DAYS)
        }
        var mesurementsListResult: MutableList<Measurement> = mutableListOf<Measurement>()
        if (res != null) {
            for ((key, mesurementList) in res) {
                var value = 0.0
                var numEl = 0
                for (measurement in mesurementList) {
                    value += measurement.value
                    numEl += 1
                }
                mesurementsListResult.add(
                    Measurement(
                        ID = "",
                        timestamp = key,
                        sensor = "",
                        plant = "",
                        value = (value/ (if (numEl > 0) numEl else 1)),
                        custom_plant_id = ""
                    )
                )
            }
        }
        when (sensorType) {
            SensorType.HUMIDITY -> humidityList.postValue(mesurementsListResult)
            SensorType.TEMPERATURE -> temperatureList.postValue(mesurementsListResult)
            SensorType.INSOLATION -> insolationList.postValue(mesurementsListResult)
            SensorType.HUMIDITYOFSOIL -> humidityOfSoilList.postValue(mesurementsListResult)
        }
    }

    fun monthlyStatistic(sensorType: SensorType, response: Response<List<Measurement>>) {
        val res = response.body()?.reversed()?.groupBy { measurement: Measurement ->
            measurement.timestamp.truncatedTo(ChronoUnit.DAYS).atZone(ZoneOffset.UTC).withDayOfMonth(1).toInstant()
        }
        var mesurementsListResult: MutableList<Measurement> = mutableListOf<Measurement>()
        if (res != null) {
            for ((key, mesurementList) in res) {
                var value = 0.0
                var numEl = 0
                for (measurement in mesurementList) {
                    value += measurement.value
                    numEl += 1
                }
                mesurementsListResult.add(
                    Measurement(
                        ID = "",
                        timestamp = key,
                        sensor = "",
                        plant = "",
                        value = (value/ (if (numEl > 0) numEl else 1)),
                        custom_plant_id = ""
                    )
                )
            }
        }
        when (sensorType) {
            SensorType.HUMIDITY -> humidityList.postValue(mesurementsListResult)
            SensorType.TEMPERATURE -> temperatureList.postValue(mesurementsListResult)
            SensorType.INSOLATION -> insolationList.postValue(mesurementsListResult)
            SensorType.HUMIDITYOFSOIL -> humidityOfSoilList.postValue(mesurementsListResult)
        }
    }

    fun getMeasurementsList(sensorType: SensorType, afterDate: Instant? = null) {
        val apiInterface = if (afterDate != null) {
            PlantMonitorService.create()
                .getMeasurementsAfterDate(sensorType.sensorName, afterDate.toString())
        } else {
            PlantMonitorService.create().getMeasurements(sensorType.sensorName)
        }
        apiInterface.enqueue(object : Callback<List<Measurement>> {
            override fun onResponse(
                call: Call<List<Measurement>>,
                response: Response<List<Measurement>>
            ) {
                when (chartType) {
                    ChartType.DAILY -> dailyStatistic(sensorType, response)
                    ChartType.WEEKLY -> weeklyStatistic(sensorType, response)
                    ChartType.MONTHLY -> monthlyStatistic(sensorType, response)
                }
            }

            override fun onFailure(call: Call<List<Measurement>>, t: Throwable) {
                Log.e("TAG", "Get measurement error")
                t.message?.let { Log.e("TAG", it) }
                errorMessage.postValue(t.message)
            }
        })
    }

    fun getSensorList() {
        val apiInterface = PlantMonitorService.create().getSensorList()
        apiInterface.enqueue(object : Callback<List<Sensor>> {
            override fun onResponse(
                call: Call<List<Sensor>>,
                response: Response<List<Sensor>>
            ) {
                sensorList.postValue(response.body())
            }

            override fun onFailure(call: Call<List<Sensor>>, t: Throwable) {
                Log.e("TAG", "Get measurement error")
                t.message?.let { Log.e("TAG", it) }
                errorMessage.postValue(t.message)
            }
        })
    }

}