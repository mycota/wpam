package com.example.wpam.ui.shopping

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.example.wpam.R
import com.example.wpam.databinding.FragmentShoppingBinding
import com.example.wpam.databinding.FragmentShoppingLidlBinding

class ShoppingFragmentLidl : Fragment() {
    private var _binding: FragmentShoppingLidlBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentShoppingLidlBinding.inflate(inflater, container, false)
        val view = binding.root
        val myWebView: WebView = binding.webview
        myWebView.loadUrl("https://www.lidl.pl/c/kwiaty/a10009213")
        myWebView.webViewClient = WebViewClient()
        return view    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}